import Api from '../base/api'


const login = (username, password) => {

    // var body = new FormData();
    // body.append('email', username, );
    // body.append('password', password);
    let body = {
        email: username,
        password: password
      };
    
    // return Api.post("/login?username=" + username + "&password=" + password);
    return Api.post("/login", body);
};  

const UserApi = {
    login
}
export default UserApi;