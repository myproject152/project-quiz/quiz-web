import React, { Component } from "react";
import "./login.css";
import "bootstrap/dist/css/bootstrap.min.css";
import UserApi from "../api/user/UserApi";
import Storage from "../storage/Storage"
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  submit = (event) => {
    event.preventDefault();
    UserApi.login(this.state.email, this.state.password).then((response) => {
      Storage.setToken(response.data.token)
      console.log(response.data.token);
    }).catch(error => {
      window.confirm(error.message);
    })
  };

  updateEmail = (evt) => {
    const val = evt.target.value;
    this.setState({
      email: val,
    });
  };

  updatePassword = (evt) => {
    const val = evt.target.value;
    this.setState({
      password: val,
    });
  };

  render() {
    return (
      <form onSubmit={this.submit}>
        <h3>Sign In</h3>
        <div className="mb-3">
          <label>Email address</label>
          <input
            type="email"
            className="form-control"
            placeholder="Enter email"
            value={this.state.email}
            onChange={this.updateEmail}
          />
        </div>
        <div className="mb-3">
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            placeholder="Enter password"
            value={this.state.password}
            onChange={this.updatePassword}
          />
        </div>
        <div className="mb-3">
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="customCheck1"
            />
            <label className="custom-control-label" htmlFor="customCheck1">
              Remember me
            </label>
          </div>
        </div>
        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
        <p className="forgot-password text-right">
          Forgot <a href="#">password?</a>
        </p>
      </form>
    );
  }
}

export default Login;
